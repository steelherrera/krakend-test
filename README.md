# krakend-poc

Project for testing microservice integration using krakend

## Run single docker container

```
docker run -p 8080:8080 -v $PWD:/etc/krakend/ \
-e FC_ENABLE=1 \
-e FC_SETTINGS="/etc/krakend/config/settings" \
devopsfaith/krakend run --config /etc/krakend/config/krakend.json
```

## Run locally using minikube

```
minikube start
eval $(minikube docker-env)
docker build -t k8s-krakend:0.0.1 .
kubectl create -f deployment-definition.yaml
kubectl create -f service-definition.yaml
minikube service krakend-service
```