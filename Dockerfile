FROM devopsfaith/krakend
COPY config /etc/krakend/config

ENV FC_ENABLE=1
ENV FC_SETTINGS="/etc/krakend/config/settings"